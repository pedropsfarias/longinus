require.config({
    waitSeconds: 120,
    paths: {
        knockout: 'libs/knockout-3.5.1',
        text: 'libs/text',
        page: 'libs/page',
        mapbox: 'libs/mapbox-gl',
        xStore: 'libs/xStore.min',
        turf: 'libs/turf.min',
        qwest: 'libs/qwest.min',
        sha256: 'libs/sha256.min'
       
    }
});

require(['app'], function (appManager) { // jshint ignore:line

    appManager.initialize();

});
