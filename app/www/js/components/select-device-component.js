define(['app', 'knockout', 'page'], function (app, ko, page) {

    return {

        template: `
            <div class="container h-100 bg-primary p-3">
                <div class="row">
                    <div class="col">
                        <img class="img-fluid" src="img/longinus.svg">  
                    </div>
                </div>
                
                    <div class="collection-header">
                        <h3 class="collection-title">
                        Selecione o dispositivo:
                        </h3>
                    </div>
                    <div data-bind="foreach: devices" class="collection">
                    <div class="collection-row collection-clickable">
                        <div class="row">
                            <div class="col-7">
                                <a data-bind="text: $data.description, click: $parent.goto" href="#" class="collection-clickable-link"></a>
                            </div>
                            <div class="col-2">
                                <a data-bind="click: $parent.edit" href="#" class="collection-clickable-link">Editar</a>
                            </div>
                            <div class="col-3">
                                <a data-bind="click: $parent.delete" href="#" class="collection-clickable-link">Apagar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <button  data-bind="click: newDevice" type="button" class="btn btn-outline-primary bg-white">Criar novo</button>
                
            </div>
        `,

        viewModel: function () {

            var self = this;

            this.devices = ko.observable([
                {
                    "id": -1,
                    "deviceType": -1,
                    "description": "Nenhum dispositivo encontrado. Criar um novo...",
                    "userId": -1
                }
            ]);

            this.listDevices = function () {

                fetch(app.apiUrl + '/device', {
                    method: 'GET',
                    headers: {
                        token: app.userStore.token()
                    }
                }).then(function (response) {

                    return response.json();

                }).then(function (data) {

                    self.devices(data);

                });

            }

            this.newDevice = function () {

                page.redirect("/create-device");

            }

            this.edit = function (device) {

                console.log(device)

                app.deviceStore.id(device.id);
                app.deviceStore.description(device.description);
                app.deviceStore.deviceType(device.deviceType);
                app.deviceStore.isEditing(true);

                page.redirect("/create-device");

            }

            this.delete = function (device) {

                fetch(app.apiUrl + '/device', {
                    method: 'DELETE',
                    body: JSON.stringify(device),
                    headers: {
                        token: app.userStore.token()
                    }
                }).then(function (response) {

                    return response.json();

                }).then(function (data) {

                    self.listDevices();

                });


            }

            this.goto = function (device) {

                if (device.id == -1) {

                    self.newDevice();

                } else {

                    let mode = 'track'
                    app.mode = mode;

                    app.deviceStore.id(device.id);
                    app.deviceStore.description(device.description);
                    app.deviceStore.deviceType(device.deviceType);
                    app.deviceStore.isEditing(false);

                    app.configStorage.set({ device });
                    app.configStorage.set({ mode });

                    app.initWatch();
                    page.redirect("/coords");

                }

            }

            this.listDevices();

        }

    }

});
