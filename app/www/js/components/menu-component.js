define(['app', 'knockout'], function (app, ko) {

    return {

        template:`
            <nav class="navbar navbar-primary">
                <div class="row w-100 m-0">
                    <div class="col m-0 p-0">
                        <img src="./img/longinus.svg" class="imgLogo" alt="" srcset="">
                    </div>
                    <ul data-bind="visible: isView" class="navbar-nav">
                        <li class="nav-item">
                            <a href="/map" class="nav-link">Mapa</a>
                        </li>
                        <li class="nav-item">
                            <a href="/devices" class="nav-link">Dispositivos</a>
                        </li>
                        <li class="nav-item">
                            <a href="/users" class="nav-link">Usuários</a>
                        </li>
                    </ul>

                </div>
                
            </nav>
        `,
        
        viewModel: function() {

            this.isView = ko.observable(app.mode == 'view')

        }
    }
  
});
