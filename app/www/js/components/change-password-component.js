define(['app', 'knockout', 'sha256', 'page'], function (app, ko, sha256, page) {

    return {

        template: `
        <div class="container h-100 bg-primary p-3">
            
        <p data-bind="visible: errorr" class="form-text text-center text-danger p-3">Senha incorreta!</p>

            <div class="row pl-3 pr-3">
                <div class="col">
                    <div class="form-group">
                        <label class="text-white" for="lc-email">Senha atual</label>
                        <input data-bind="value: password" type="password" class="form-control" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;">
                    </div>    
                </div>
            </div>
            <div class="row pl-3 pr-3">
                <div class="col">
                    <div class="form-group">
                        <label class="text-white" for="lc-email">Nova senha</label>
                        <input data-bind="value: password1" type="password" class="form-control" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;">
                    </div>    
                </div>
            </div>
            <div class="row pl-3 pr-3">
                <div class="col">
                    <div class="form-group">
                        <label class="text-white" for="lc-email">Repita a senha</label>
                        <input data-bind="value: password2" type="password" class="form-control" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;">
                    </div>    
                </div>
            </div>
            <div class="row pl-3 pr-3 pt-3">
                <div class="col">
                    <div class="form-group">
                        <button data-bind="click: login" type="button" class="btn btn-secondary btn-block">Trocar senha</button>
                    </div>    
                </div>
            </div>
        </div>
        `,

        viewModel: function () {

            var self = this;
           
            this.password = ko.observable('');
            this.password1 = ko.observable('');
            this.password2 = ko.observable('');
            this.isValid = ko.observable(false);
            this.errorr = ko.observable(false);

            login = function () {

                self.isValid((self.password() != '' && self.password1() == self.password2()));

                if(self.isValid()){

                    let u = {
                        id: app.userStore.id()
                    };

                    fetch(app.apiUrl + '/password', {
                        method: 'PUT',
                        body: JSON.stringify(u),
                        headers: {
                            token: app.userStore.token(),
                            hash: sha256(self.password()),
                            newhash: sha256(self.password1())
                        }
                    }).then(function (response) {
        
                        return response.json();
        
                    }).then(function (data) {

                        if(data.success){

                            page.redirect('/users')

                        } else {

                          
                            page.redirect('/change-password')

                        }

                    });

                }

                

               

            }

            validate = function () {

               // console.log((self.password() != '' && self.password1() == self.password2()))

                self.isValid(
                    self.password() != '' && 
                    self.password1() != '' && 
                    self.password1() == self.password2()
                );

               // return (self.password() != '' && self.password1() == self.password2());

            }

        }
    }

});
