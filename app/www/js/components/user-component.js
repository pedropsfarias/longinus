define(['app', 'knockout', 'page'], function (app, ko, page) {

    return {

        template: `
            <div class="container h-100 bg-primary p-3">

                    <div class="tile p-4 mb-4 tile-light" style="width: 100%">
                        <div class="row">

                            <div class="col"><a href="/change-password">Trocar senha</a></div>
                            <div class="col"><a href="/logout">Sair</a></div>
                        
                        </div>
                    </div>


                    <div class="collection-header">
                        <h3 class="collection-title">
                        Usuários
                        </h3>
                    </div>
                    <div data-bind="foreach: users" class="collection">
                    <div class="collection-row collection-clickable">
                        <div class="row">
                            <div class="col-7">
                                <a data-bind="text: $data.name" href="#" class="collection-clickable-link"></a>
                            </div>
                            <div class="col-2">
                                <a data-bind="click: $parent.edit" href="#" class="collection-clickable-link">Editar</a>
                            </div>
                            <div class="col-3">
                                <a data-bind="click: $parent.delete" href="#" class="collection-clickable-link">Apagar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <button  data-bind="click: newUser" type="button" class="btn btn-outline-primary bg-white">Criar novo</button>
                
            </div>
        `,

        viewModel: function () {

            var self = this;

            this.users = ko.observable([
                {
                    "id": -1,
                    "name": "Nenhum dispositivo encontrado. Criar um novo...",
                    "email": "Nenhum dispositivo encontrado. Criar um novo...",
                }
            ]);

            this.listUsers = function () {

                fetch(app.apiUrl + '/user', {
                    method: 'GET',
                    headers: {
                        token: app.userStore.token()
                    }
                }).then(function (response) {

                    return response.json();

                }).then(function (data) {

                    self.users(data);

                });

            }

            this.newUser = function () {

                page.redirect("/create-user");

            }

            this.edit = function (user) {

                console.log(user)

                app.userEditStore.id(user.id);
                app.userEditStore.name(user.name);
                app.userEditStore.email(user.email);
                app.userEditStore.isEditing(true);

                page.redirect("/create-user");

            }

            this.delete = function (device) {

                fetch(app.apiUrl + '/user', {
                    method: 'DELETE',
                    body: JSON.stringify(device),
                    headers: {
                        token: app.userStore.token()
                    }
                }).then(function (response) {

                    return response.json();

                }).then(function (data) {

                    self.listUsers();

                });


            }

            this.listUsers();

        }

    }

});
