define(['app', 'knockout', 'page'], function (app, ko, page) {

    return {

        template:`
        <div class="container h-100  p-0 m-0">
            <div class="row p-3 ">
                <div class="col h-100 coords-info">
                    <div class="tile tile-white text-left w-100 h-100">
                        <table class="table table-align-middle m-0 h-100 w-100">
                        <tbody>
                        <tr>
                            <td class="font-weight-bold text-right">Status</th>
                            <td><span data-bind="text:status"></span></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold text-right">Coordenadas</th>
                            <td>(<span data-bind="text:latitude"></span>, <span data-bind="text:longitude"></span>) ±<span data-bind="text:accuracy"></span>m</td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold text-right">Altitude</th>
                            <td><span data-bind="text:altitude"></span>m ±<span data-bind="text:altitudeAccuracy"></span>m</td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold text-right">Velocidade</th>
                            <td><span data-bind="text:speed"></span>m/s</td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold text-right">Direção</th>
                            <td><span data-bind="text:heading"></span>°</td>
                        </tr>
                        </tbody>
                        </table>
                       

                    </div>
                </div>
            </div>
        </div>

        <div class="map-coords bg-white">
                <button data-bind="event: { click: toMap }" type="button" class="btn btn-outline-primary">Ver mapa</button>
            </div>
        `,
        
        viewModel: function() {

            var self = this;

            this.latitude = ko.observable(app.positionStore.latitude());
            this.longitude = ko.observable(app.positionStore.longitude());
            this.altitude = ko.observable(app.positionStore.altitude()); 
            this.accuracy = ko.observable(app.positionStore.accuracy()); 
            this.altitudeAccuracy = ko.observable(app.positionStore.altitudeAccuracy());
            this.heading = ko.observable(app.positionStore.heading());
            this.speed = ko.observable(app.positionStore.speed());
            this.timestamp = ko.observable(app.positionStore.timestamp());
            this.status = ko.observable(app.positionStore.status());

            self.latitudeSubscribe = app.positionStore.latitude.subscribe(function (val) {
                
                val = val ? val.toFixed(5) : ' - ';
                self.latitude(val);
                
            }, this);

            self.longitudeSubscribe = app.positionStore.longitude.subscribe(function (val) {
                
                val = val ? val.toFixed(5) : ' - ';
                self.longitude(val);

            }, this);

            self.altitudeSubscribe = app.positionStore.altitude.subscribe(function (val) {

                val = val ? val.toFixed(1) : ' - ';
                self.altitude(val);

            }, this);

            self.accuracySubscribe = app.positionStore.accuracy.subscribe(function (val) {

                val = val ? val.toFixed(1) : ' - ';
                self.accuracy(val);

            }, this);

            self.altitudeAccuracySubscribe = app.positionStore.altitudeAccuracy.subscribe(function (val) {

                val = val ? val.toFixed(1) : ' - ';
                self.altitudeAccuracy(val);

            }, this);

            self.headingSubscribe = app.positionStore.heading.subscribe(function (val) {

                val = val ? val.toFixed(4) : ' - ';
                self.heading(val);

            }, this);

            self.speedSubscribe = app.positionStore.speed.subscribe(function (val) {

                val = val ? val.toFixed(1) : ' - ';
                self.speed(val);

            }, this);

            self.statusSubscribe = app.positionStore.status.subscribe(function (val) {

                self.status(val);

            }, this);
            

            // self.timestampSubscribe = app.positionStore.timestamp.subscribe(function (val) {
            //     self.timestamp(val);
            // }, this);

            self.toMap = function(){

                page.redirect("/map");

            }

        }
    }
  
});
