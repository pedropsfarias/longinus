define(['app', 'knockout', 'page', 'sha256'], function (app, ko, page, sha256) {

    return {

        template: `
            <div class="container h-100 bg-primary p-3">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Nome</label>
                            <input data-bind="value: name" type="text" class="form-control" placeholder="Nome">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Email</label>
                            <input data-bind="value: email" type="email" class="form-control" placeholder="Email">
                        </div>
                    </div>
                </div>
                <div data-bind="hidden: isEditing" class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Senha</label>
                            <input data-bind="value: password" type="password" class="form-control" placeholder="Senha">
                        </div>
                    </div>
                </div>
                <button  data-bind="click: create" type="button" class="btn btn-outline-primary bg-white">Salvar</button>
                
            </div>
        `,

        viewModel: function () {

            var self = this;

            self.id = ko.observable(app.userEditStore.id());
            self.name = ko.observable(app.userEditStore.name());
            self.email = ko.observable(app.userEditStore.email());
            self.password = ko.observable(app.userEditStore.hash());
            self.isEditing = ko.observable(app.userEditStore.isEditing());

            this.create = function () {

                let user = {
                    id: self.id(),
                    name: self.name(),
                    email: self.email(),
                    hash: sha256(self.password())
                };

                fetch(app.apiUrl +'/user', {
                    method: app.userEditStore.isEditing() ? 'PUT' : 'POST',
                    body: JSON.stringify(user),
                    headers:{
                        token: app.userStore.token()
                    }
                }).then(function (response) {

                    return response.json();

                }).then(function (data) {

                    page.redirect('/users');

                });

                app.userEditStore.isEditing(false);
                app.userEditStore.id(-1);
                app.userEditStore.name('');
                app.userEditStore.email('');

            }

        }
        
    }

});
