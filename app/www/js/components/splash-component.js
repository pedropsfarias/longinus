define(['app', 'knockout'], function (appManager, ko) {

    return {

        template:`
            <div class="splash">
                <loader-component></loader-component>
            </div>
        `,
        viewModel: function(){

            setTimeout(()=>{
                appManager._removeComponent('splash-component');
            }, 3000);

        }
        
    }
  
});

