define(['app', 'knockout', 'page'], function (app, ko, page) {

    return {

        template: `
            <div class="container h-100 bg-primary p-3">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Selecione o tipo do dispositivo</label>
                            <select data-bind="value: deviceType" class="form-control">
                                <option value="-1">Selecione...</option>
                                <option value="1">Moto</option>
                                <option value="2">Carro</option>
                                <option value="3">Caminhão</option>
                                <option value="4">Objeto</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label>Descrição</label>
                            <input data-bind="value: description" type="text" class="form-control" placeholder="Descrição">
                        </div>
                    </div>
                </div>
                <button  data-bind="click: create" type="button" class="btn btn-outline-primary bg-white">Salvar</button>
                
            </div>
        `,

        viewModel: function () {

            var self = this;

            self.id = ko.observable(app.deviceStore.id());
            self.deviceType = ko.observable(app.deviceStore.deviceType());
            self.description = ko.observable(app.deviceStore.description());

            this.create = function () {

                let device = {
                    id: self.id(),
                    deviceType: self.deviceType(),
                    description: self.description()
                };

                fetch(app.apiUrl +'/device', {
                    method: app.deviceStore.isEditing() ? 'PUT' : 'POST',
                    body: JSON.stringify(device),
                    headers:{
                        token: app.userStore.token()
                    }
                }).then(function (response) {

                    return response.json();

                }).then(function (data) {

                    page.redirect('/select-device');

                });

                app.deviceStore.isEditing(false);
                app.deviceStore.id(-1);
                app.deviceStore.deviceType(-1);
                app.deviceStore.description('');

            }

        }
        
    }

});
