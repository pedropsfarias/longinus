define(['app', 'knockout', 'mapbox', 'turf', 'page'], function (app, ko, mapboxgl, turf, page) {

    return {

        template: `
        <div id="map">
            <div data-bind="visible: trackMode" class="map-info text-success"><span data-bind="text:status"></span></div>
            <div data-bind="visible: trackMode" class="map-coords">
                <button data-bind="event: { click: toCoords }" type="button" class="btn btn-outline-primary">Mais informações</button>
            </div>
        </div>`,

        viewModel: function () {

            var self = this;

            self.latitude = ko.observable(app.positionStore.latitude());
            self.longitude = ko.observable(app.positionStore.longitude());
            self.accuracy = ko.observable(app.positionStore.accuracy());
            self.status = ko.observable(app.positionStore.status());
            self.zoom = ko.observable();
            self.trackMode = ko.observable(app.mode == 'track');
            self.icons = [
                {
                    id: 1,
                    icon: 'moto.png'
                },
                {
                    id: 2,
                    icon: 'carro.png'
                },
                {
                    id: 3,
                    icon: 'caminhao.png'
                },
                {
                    id: 4,
                    icon: 'box.png'
                }
            ]

            self.latitudeSubscribe = app.positionStore.latitude.subscribe(function (val) {
                self.latitude(val);
                self._bindMapPosition();
            }, this);

            self.longitudeSubscribe = app.positionStore.longitude.subscribe(function (val) {
                self.longitude(val);
                //self._bindMapPosition();
            }, this);

            self.accuracySubscribe = app.positionStore.accuracy.subscribe(function (val) {
                self.accuracy(val);
                //self._bindMapPosition();
            }, this);

            self.statusSubscribe = app.positionStore.status.subscribe(function (val) {
                self.status(val);
                self._bindMapPosition();
            }, this);

            self._getIcons = function () {

                self._map.on('load', () => {

                    for (let i = 0; i < self.icons.length; i++) {

                        let {id, icon} = self.icons[i];

                        self._map.loadImage('img/map/' + icon,
                            function (error, image) {
                                if (error) throw error;
                                self._map.addImage(id, image);
                            }
                        );
                    }

                });

            }

            self._initLayers = function () {

                self._map.on('load', () => {

                    self._map.loadImage(
                        'img/map/moto.png',
                        function (error, image) {
                            if (error) throw error;
                            self._map.addImage('cat', image);
                        });

                    self._map.addSource('buffer', {
                        'type': 'geojson',
                        'data': {
                            'type': 'Feature',
                            'geometry': {
                                'type': 'Polygon',
                                'coordinates': [
                                    [
                                        [1000, 900],
                                        [900, 1000],
                                        [1000, 1000],
                                        [1000, 900],
                                        [900, 900]
                                    ]
                                ]
                            }
                        }
                    });

                    self._map.addSource('point', {
                        'type': 'geojson',
                        'data': {
                            'type': 'Point',
                            'coordinates': [1000, 1000]
                        }
                    });

                    self._map.addLayer({
                        'id': 'buffer',
                        'source': 'buffer',
                        'type': 'fill',
                        'paint': {
                            'fill-color': '#088',
                            'fill-opacity': 0.1
                        }
                    });

                    self._map.addLayer({
                        'id': 'point',
                        'source': 'point',
                        'type': 'circle',
                        'paint': {
                            'circle-radius': 12,
                            'circle-color': '#fff'
                        }
                    });

                    self._map.addLayer({
                        'id': 'poi-labels',
                        'type': 'symbol',
                        'source': 'point',
                        'layout': {
                            'text-field': ['get', 'description'],
                            'text-variable-anchor': ['left'],
                            'text-radial-offset': 1,
                            'text-justify': 'auto',
                            'text-size': 12,
                            'icon-image': ['get', 'deviceType'],
                            'icon-size': 0.3
                        }
                    });

                    self._map.on('click', 'point', function (e) {

                        let cnt = self.createPopUpContent(e.features[0].properties);

                        new mapboxgl.Popup()
                        .setLngLat(e.lngLat)
                        .setHTML(cnt)
                        .addTo(self._map);

                    });

                    self._viewPositions();

                });

            }

            self._viewPositions = function () {

                if (app.mode == 'view') {

                    self._getPositions(true);
                    setInterval(self._getPositions, 10000);

                }

            }

            self._getPositions = function (zoom) {

                fetch(app.apiUrl + '/position', {
                    method: 'GET',
                    headers: {
                        token: app.userStore.token()
                    }
                }).then(function (response) {

                    return response.json();

                }).then(function (data) {

                    self._drawPositions(data, zoom);

                });



            }

            self._drawPositions = function (data, zoom) {

                let features = [];

                for (let i = 0; i < data.length; i++) {

                    const pt = data[i];
                    const { deviceType, description } = app.devices().find(function (d) {
                        return d.id == pt.device_id;
                    })
                    let feature = {
                        'type': 'Feature',
                        'geometry': {
                            'type': 'Point',
                            'coordinates': [
                                pt.longitude,
                                pt.latitude
                            ]
                        },
                        'properties': {
                            ...pt,
                            deviceType,
                            description
                        }
                    }

                    features.push(feature);

                }

                let featCollection = {
                    'type': 'FeatureCollection',
                    'features': features
                }

                if (zoom) {

                    let bbox = turf.bbox(featCollection);
                    self._map.fitBounds(bbox, {
                        padding: 100
                    });

                }

                self._map.getSource('point').setData(featCollection);

            }

            self._bindMapPosition = function () {

                if (self._map.getSource('buffer')) {

                    var point = turf.point([self.longitude(), self.latitude()]);
                    var buffered = self._generateBuffer(point, self.accuracy());
                    var bbox = turf.bbox(buffered);

                    self._map.getSource('buffer').setData(buffered);
                    self._map.getSource('point').setData(point.geometry);

                    self._map.setCenter([self.longitude(), self.latitude()]);

                    self._map.fitBounds(bbox, {
                        padding: 100
                    });

                }

            }

            self._generateBuffer = function (feature, radius) {

                var buffered = turf.buffer(feature, radius * 1.2 || 100, { units: 'meters', steps: 1024 });
                return buffered;

            }

            self.toCoords = function () {

                page.redirect("/coords");
                self.zoom(self._map.getZoom())

            }

            self.createPopUpContent = function (feature) {

                let lastUpdate = new Date(Number(feature.timestamp)).toLocaleString();

                return `
                    <div class="tile p-3 tile-light text-left" style="width: 100%">
                        <h4>${feature.description || ''}</h4>
                        <span class="font-weight-bold">Latitude:</span> ${feature.latitude}<br>
                        <span class="font-weight-bold">Longitude:</span> ${feature.longitude}<br>
                        <span class="font-weight-bold">Atualiz.:</span> ${lastUpdate}<br>
                    </div>
                
                `;


            }

            mapboxgl.accessToken = 'pk.eyJ1IjoicGVkcm9wc2ZhcmlhcyIsImEiOiJjaWg5NXQxMWkwdHNjdWprbHloZWF0eDhwIn0.wN3tEcfQsgxUQyn4qq3YNw';

            self._map = new mapboxgl.Map({
                container: 'map', // container id
                style: 'mapbox://styles/mapbox/outdoors-v11', // stylesheet location
                center: self.latitude() ? [self.longitude(), self.latitude()] : [0, 0], // starting position [lng, lat]
                zoom: self.zoom() || 1 // starting zoom
            });

            this._getIcons();
            this._initLayers();
            

        }

    }

});