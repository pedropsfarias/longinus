define(['app', 'knockout', 'page'], function (app, ko, page) {

    return {

        template:`
            <div class="container h-100 bg-primary p-3">
                <div class="row">
                    <div class="col">
                        <img class="img-fluid" src="img/longinus.svg">  
                    </div>

                </div>
                <div class="row pl-3 pr-3 pt-3">
                    <div class="col">
                        <div class="form-group">
                            <button  data-bind="click: track" type="button" class="btn btn-secondary btn-block">Rastrear este dispositivo</button>
                        </div>    
                    </div>
                </div>
                <div class="row pl-3 pr-3 pt-3">
                    <div class="col">
                        <div class="form-group">
                            <button  data-bind="click: view" type="button" class="btn btn-secondary btn-block">Visualizar meus dispositivos</button>
                        </div>    
                    </div>
                </div>
            </div>
        `,
        
        viewModel: function() {

            this.track = function(){
                

                app.mode = 'track';
                page.redirect("/select-device");
                app._listDevices();

            }

            this.view = function(){

                let mode = 'view';
                app.mode = mode;
                app.configStorage.set({ mode });
                page.redirect("/map");
                app._listDevices();

            }


        }
    }
  
});
