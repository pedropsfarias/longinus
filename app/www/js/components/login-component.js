define(['app', 'knockout', 'sha256', 'page'], function (app, ko, sha256, page) {

    return {

        template: `
        <div class="fixed-top container h-100 bg-primary p-3">
            <div class="row">
                <div class="col">
                    <img class="img-fluid" src="img/longinus.svg">  
                </div>

            </div>
            <div class="row pl-3 pr-3">
                <div class="col">
                    <p data-bind="visible: error" class="form-text text-center text-danger">Usuário ou senha incorretos!</p>
                    <div class="form-group">
                        <label class="text-white" for="lc-email">Email</label>
                        <input data-bind="value: email" type="email" class="form-control"  placeholder="seu@email.com">
                    </div>    
                </div>
            </div>
            <div class="row pl-3 pr-3">
                <div class="col">
                    <div class="form-group">
                        <label class="text-white" for="lc-email">Senha</label>
                        <input data-bind="value: password" type="password" class="form-control" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;">
                    </div>    
                </div>
            </div>
            <div class="row pl-3 pr-3 pt-3">
                <div class="col">
                    <div class="form-group">
                        <button data-bind="click: login, enabled: validate()" type="button" class="btn btn-secondary btn-block">Login</button>
                    </div>    
                </div>
            </div>
        </div>
        `,

        viewModel: function () {

            email = ko.observable('root@longinus');
            password = ko.observable('123');
            error = ko.observable(false);

            login = function () {

                var xhr = new XMLHttpRequest();
                xhr.open('POST', app.apiUrl + '/auth', true);
                xhr.setRequestHeader("email", email());
                xhr.setRequestHeader("hash", sha256(password()));
                xhr.send();
                xhr.onreadystatechange = function () {

                    if (xhr.readyState == 4) {

                        if (xhr.status == 200) {

                            var user = JSON.parse(xhr.responseText);

                            app.userStore.id(user.id);
                            app.userStore.name(user.name);
                            app.userStore.email(user.email);
                            app.userStore.token(user.token);

                            app.configStorage.set({
                                user: user,
                                status: 'OK'
                            });

                            app._removeComponent('login-component')

                            page.redirect("/mode");

                        } else {

                            error(true);

                        }

                    }

                }

            }

            validate = function () {

                console.log(email() && senha())

                return (email() && senha());
            }

        }
    }

});
