define(['knockout', 'page', 'xStore'], function (ko, page, xs) { // jshint ignore:line

    return {

        apiUrl: 'http://192.168.0.104:4567',

        bodyElm: document.getElementsByTagName('body')[0],

        loadedComponents: [],

        configStorage: new xs.xStore("config", localStorage),

        userStore: {
            id: ko.observable(-1),
            name: ko.observable(""),
            email: ko.observable(""),
            token: ko.observable("")
        },

        deviceStore: {
            id: ko.observable(-1),
            description: ko.observable(""),
            deviceType: ko.observable(-1),
            isEditing: ko.observable(false)
        },

        devices: ko.observable([]),

        userEditStore: {
            id: ko.observable(-1),
            name: ko.observable(""),
            email: ko.observable(""),
            hash: ko.observable(""),
            isEditing: ko.observable(false)
        },

        mode: ko.observable(""),

        positionStore: {
            latitude: ko.observable(0),
            latitude: ko.observable(0),
            longitude: ko.observable(0),
            altitude: ko.observable(0),
            accuracy: ko.observable(0),
            altitudeAccuracy: ko.observable(0),
            heading: ko.observable(0),
            speed: ko.observable(0),
            timestamp: ko.observable(0),
            device_id: ko.observable(0),
            status: ko.observable('Obtendo coordenadas...')
        },

        initialize: function () {

            var self = this;

            self._registerComponents();
            self._registerRoutes();
            self._initialize();
            self._listDevices();

            document.addEventListener("deviceready", function () {

                if (cordova.platformId == 'android') {

                    cordova.plugins.backgroundMode.enable();
                    StatusBar.hide();

                }

            }, false);

        },

        _registerRoutes: function () {


            page('/login', () => {

                this._addComponent('login-component', this.bodyElm, true);

            });

            page('/mode', this._loginHandler, () => {

                this._removeAllComponents();
                this._addComponent('mode-component', false, true);

            });

            page('/select-device', this._loginHandler, () => {

                this._removeAllComponents();
                this._addComponent('select-device-component', false, true);

            });

            page('/create-device', this._loginHandler, () => {

                this._removeAllComponents();
                //this._addComponent('create-device-component', false, true);
                this._addComponents(['menu-component', 'create-device-component']);


            });

            page('/map', this._loginHandler, () => {

                this._removeAllComponents();
                this._addComponents(['menu-component', 'map-component']);

            });

            page('/coords', this._loginHandler, () => {

                this._removeAllComponents();
                this._addComponents(['menu-component', 'coords-component']);

            });

            page('/users', this._loginHandler, () => {

                this._removeAllComponents();
                this._addComponents(['menu-component', 'user-component']);

            });

            page('/create-user', this._loginHandler, () => {

                this._removeAllComponents();
                this._addComponents(['menu-component', 'create-user-component']);

            });

            page('/devices', this._loginHandler, () => {

                this._removeAllComponents();
                this._addComponents(['menu-component', 'device-component']);

            });

            page('/change-password', this._loginHandler, () => {

                this._removeAllComponents();
                this._addComponents(['menu-component', 'change-password-component']);

            });

            page('/logout', this._loginHandler, () => {

                this._logout();

            });

            page('*', this._loginHandler, () => {

                if (this.mode == 'track') {

                    page.redirect("/coords");

                } else {

                    page.redirect("/map");

                }

            });

            page({
                hashbang: true
            });


        },

        _registerComponents: function () {

            ko.components.register('menu-component', { require: 'components/menu-component' });
            ko.components.register('loader-component', { require: 'components/loader-component' });
            ko.components.register('splash-component', { require: 'components/splash-component' });
            ko.components.register('login-component', { require: 'components/login-component' });
            ko.components.register('config-component', { require: 'components/config-component' });
            ko.components.register('map-component', { require: 'components/map-component' });
            ko.components.register('coords-component', { require: 'components/coords-component' });
            ko.components.register('mode-component', { require: 'components/mode-component' });
            ko.components.register('select-device-component', { require: 'components/select-device-component' });
            ko.components.register('create-device-component', { require: 'components/create-device-component' });
            ko.components.register('user-component', { require: 'components/user-component' });
            ko.components.register('create-user-component', { require: 'components/create-user-component' });
            ko.components.register('device-component', { require: 'components/device-component' });
            ko.components.register('change-password-component', { require: 'components/change-password-component' });

            ko.options.useOnlyNativeEvents = true;
            ko.applyBindings();
        },

        _initialize: function () {

            if (this.configStorage.get('status')) {

                let user = this.configStorage.get('user');
                let device = this.configStorage.get('device');
                this.mode = this.configStorage.get('mode');

                this.userStore.id(user.id);
                this.userStore.name(user.name);
                this.userStore.email(user.email);
                this.userStore.token(user.token);

                if (this.mode == 'track') {

                    this.deviceStore.id(device.id);
                    this.deviceStore.description(device.description);
                    this.deviceStore.deviceType(device.deviceType);
                    this.deviceStore.isEditing(false);

                    this.initWatch();
                    page.redirect("/coords");

                } else {

                    //page.redirect("/map");

                }

                return;

            } else {

                this.configStorage.set({
                    coordinateInterval: 30000
                });

                page.redirect("/login");

            }

        },

        _loginHandler: (ctx, next) => {

            let configStorage = new xs.xStore("config", localStorage);

            if (configStorage) {

                if (configStorage.get('status') == 'OK') next();

            } else {

                page.redirect("/login");

            }


        },

        _addComponent: function (component, location, applyBindings) {

            var elm = location || document.getElementById('container');
            elm.innerHTML += '<' + component + '></' + component + '>';
            this.loadedComponents.push(component);

            if (applyBindings) this._applyBindings();

        },

        _addComponents: function (components) {

            for (let i = 0; i < components.length; i++) {
                this._addComponent(components[i]);
            }

            this._applyBindings();

        },

        _removeComponent: function (component) {

            let elms = document.getElementsByTagName(component);
            if (elms.length > 0) {
                elms[0].remove();
            }

        },

        _removeAllComponents: function () {

            document.getElementById('container').innerHTML = '';
            this.loadedComponents = [];

        },

        _applyBindings: function () {

            for (let i = 0; i < this.loadedComponents.length; i++) {

                let component = this.loadedComponents[i];
                let elms = document.getElementsByTagName(component);
                if (elms.length > 0) ko.applyBindings({}, elms[0]);

            }

        },

        _listDevices: function () {

            let self = this;

            let configStorage = new xs.xStore("config", localStorage);

            if (configStorage.get('status') == 'OK') {

                fetch(this.apiUrl + '/device', {
                    method: 'GET',
                    headers: {
                        token: this.userStore.token()
                    }
                }).then(function (response) {

                    return response.json();

                }).then(function (data) {

                    self.devices(data);

                });

            }

        },

        initWatch: function () {

            var self = this;

            self._getGeolocation();

            clearInterval(self.geolocationInterval);
            self.geolocationInterval = setInterval(function () {

                self._getGeolocation();

            }, this.configStorage.get('coordinateInterval') || 120000);

        },

        _getGeolocation() {

            var self = this;

            navigator.geolocation.getCurrentPosition(
                function (p) {
                    console.log("resposta")
                    self._handleWatchPosition(p)
                },
                function () {

                    console.log("erro")
                },
                {
                    maximumAge: 1000,
                    timeout: 10000,
                    enableHighAccuracy: true
                }
            );

        },

        _handleWatchPosition: function (position) {

            let self = this;

            this.positionStore.latitude(position.coords.latitude);
            this.positionStore.longitude(position.coords.longitude);
            this.positionStore.altitude(position.coords.altitude);
            this.positionStore.accuracy(position.coords.accuracy);
            this.positionStore.altitudeAccuracy(position.coords.altitudeAccuracy);
            this.positionStore.heading(position.coords.heading);
            this.positionStore.speed(position.coords.speed);
            this.positionStore.timestamp(Date.now());
            this._handleStatus(Date.now());

            let p = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                altitude: position.coords.altitude,
                accuracy: position.coords.accuracy,
                altitudeAccuracy: position.coords.altitudeAccuracy,
                heading: position.coords.heading,
                speed: position.coords.speed,
                timestamp: position.timestamp,
                device_id: this.deviceStore.id()
            }

            fetch(this.apiUrl + '/position', {
                method: 'POST',
                body: JSON.stringify(p),
                headers: {
                    token: this.userStore.token()
                }
            }).then(function (response) {

                return response.json();

            }).then(function (data) {

                console.log('Persistiu: ', data)

            });


        },

        _handleStatus: function (timestamp) {

            var self = this;

            var deadTimestamp = timestamp + this.configStorage.get('coordinateInterval');


            if (timestamp) {

                var time = 0;
                var lostTime = 0;
                clearInterval(self.stutusInterval);
                self.stutusInterval = setInterval(function () {

                    if (Date.now() < deadTimestamp) {

                        self.positionStore.status('Atualizado (' + time + 's)');
                        time++;

                    } else {

                        self.positionStore.status('Aguardando (' + lostTime + 's)');
                        lostTime++;

                    }

                }, 1000)

            } else {

                self.status('Sinal GPS perdido...');

            }

        },

        _logout: function () {

            localStorage.clear();
            page.redirect('/login');

        }

    };
});