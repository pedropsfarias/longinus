class Longinus {

    constructor(config) {

        this._map = config.map;

        this._watchID = null;
        this._serverApi = 'http://192.168.52.1:4567';
        this._deviceType = device.platform || 'browser';

        this._initLayers();

    }

    _initLayers() {

        this._map.on('load', () => {

            this._map.addSource('buffer', {
                'type': 'geojson',
                'data': {
                    'type': 'Feature',
                    'geometry': {
                        'type': 'Polygon',
                        'coordinates': [
                            [
                                [0, 0],
                                [0, 1],
                                [1, 1],
                                [1, 0],
                                [0, 0]
                            ]
                        ]
                    }
                }
            });

            this._map.addSource('point', {
                'type': 'geojson',
                'data': {
                    'type': 'Point',
                    'coordinates': [0, 0]
                }
            });

            this._map.addLayer({
                'id': 'buffer',
                'source': 'buffer',
                'type': 'fill',
                'paint': {
                    'fill-color': '#088',
                    'fill-opacity': 0.1
                }
            });

            this._map.addLayer({
                'id': 'point',
                'source': 'point',
                'type': 'circle',
                'paint': {
                    'circle-radius': 10,
                    'circle-color': '#007cbf'
                }
            });

            this._initApp();

        });


    }

    _initApp() {

        if (this._deviceType !== 'browser') {

            this._watchID = navigator.geolocation.watchPosition((p) => this._handleWatchPosition(p), onError, {
                maximumAge: 3000,
                timeout: 5000,
                enableHighAccuracy: true
            });

        } else {
            this._handleShowPositions();
            splash.hide();
        }



    }

    _handleWatchPosition(position) {

        this._bindMapPosition(position);
        this._bindPositionUi(position);
        this._sendPosition(position);

    }

    _handleShowPositions(){

        this._getPositions();

    }

    _generateBuffer(feature, radius) {

        var buffered = turf.buffer(feature, radius || 100, { units: 'meters', steps: 1024 });
        return buffered;

    }

    _bindMapPosition(position){

        let { longitude, latitude, accuracy } = position.coords;

        var point = turf.point([longitude, latitude]);
        var buffered = this._generateBuffer(point, accuracy);
        var bbox = turf.bbox(buffered);

        this._map.getSource('buffer').setData(buffered);
        this._map.getSource('point').setData(point.geometry);


        // marker.setLngLat([position.coords.longitude, position.coords.latitude]);

        this._map.setCenter([longitude, latitude]);
        //marker.addTo(map);

        // this._map.fitBounds(bbox, {
        //     padding: 20
        // });



    }
    
    _bindPositionUi(position){

        document.getElementById('latitude').value = position.coords.latitude;
        document.getElementById('longitude').value = position.coords.longitude;
        document.getElementById('altitude').value = position.coords.altitude;
        document.getElementById('precision').value = position.coords.accuracy;

        splash.hide();

    }

    _sendPosition(position) {

        var data = {};
        data.latitude = position.coords.latitude;
        data.longitude = position.coords.longitude;
        data.altitude = position.coords.altitude;
        data.accuracy = position.coords.accuracy;
        data.altitudeAccuracy = position.coords.altitudeAccuracy;
        data.heading = position.coords.heading;
        data.speed = position.coords.speed;
        data.timestamp = position.timestamp;

        $.ajax({
            method: "POST",
            url: this._serverApi + "/position",
            data: JSON.stringify(data),
            dataType: 'json',
            contentType: 'application/json',
        });


    }

    _getPositions(){

        $.ajax({
            method: "GET",
            url: this._serverApi + "/position",
            dataType: 'json',
            contentType: 'application/json',
            success: (data) => {
                console.log(data);
            }
        });


    }
}

function onError(error) {
    alert('code: ' + error.code + '\n' +
        'message: ' + error.message + '\n');
}

