-- drop database longinus;
create database longinus;

-- drop table users
create table users(
	id serial primary key not null, 
	name text,
	email text,
	hash text
);

-- drop table devices
create table devices(
	id serial primary key not null, 
	device_type int,
	description text,
	user_id int REFERENCES users(id)
);

-- drop table errors
create table errors(
	id serial primary key not null, 
	error_type int,
	description text,
	device_id int REFERENCES devices(id)
	
);

-- drop table positions
create table positions(
	id bigserial primary key not null, 
	latitude double precision not null,
	longitude double precision not null,
	altitude real,
	accuracy real not null,
	altitudeAccuracy real,
	heading real,
	speed real,
	timestamp timestamp,
	device_id REFERENCES devices(id)
);



insert into users (name, email, hash) values ('Administrator', 'root@longinus','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3')
